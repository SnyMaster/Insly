<?php
namespace insly;

use insly\DataInterface;

class Form implements DataInterface
{
        
    private $errors = [];
    private $data = [];
    
    public function getEstimate() {
        return $this->estimate;   
    }
    
    public function getInstalments() {
        return $this->instalments;
    }
    
    public function getTax() {
        return $this->tax;
    }
    
    public function getUserDay() {
        return $this->userDay;
    }
    
    public function getUserHour() {
        return $this->userHour;
    }
    
    public function __get($name) {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return NULL;
    }

    public function load(array $formdata)
    {
        $listAttributes = array_keys($this->attributes());
        $this->clearErrors();
        foreach ($listAttributes as $attr) {
            if (isset($formdata[$attr])) {
                $this->data[$attr] = $this->getDataFromOut($attr, $formdata[$attr]);
            }
        }
        return $this->validate();
    }
    
    public function hasError()
    {
        return !empty($this->errors);
    }
    
    public function getErrors()
    {
        return $this->errors;
    }
    
    public function validate()
    {
      
        $this->clearErrors();
        foreach ($this->attributes() as $attrName =>$attr) {
            if (isset($attr['required']) && $attr['required'] && $this->$attrName === NULL) {
                $this->addError($attrName, "No set required param '$attrName'");
            }
            if ($this->$attrName !== NULL && in_array($attr['type'], ['integer', 'number']) &&
                    isset($attr['min']) && $attr['min'] > $this->$attrName) {
                $this->addError($attrName, "MIN value for '$attrName' is {$attr['min']}");
            }
            if ($this->$attrName !== NULL && in_array($attr['type'], ['integer', 'number']) &&
                    isset($attr['max']) && $attr['max'] < $this->$attrName) {
                $this->addError($attrName, "MAX value for '$attrName' is {$attr['max']}");
            }
        }
        return !$this->hasError();
    }
        
    private function attributes()
    {
        return [
            'estimate' => ['type' => 'integer', 'min' => 100, 'max' => 100000, 'required' => TRUE],
            'tax' => ['type' => 'integer', 'min' => 1, 'max' => 100, 'required' => TRUE],
            'instalments' => ['type' => 'integer', 'min' => 1, 'max' => 12, 'required' => TRUE],
            'userDay' => ['type' => 'integer', 'min' => 0, 'max' => 6, 'required' => TRUE],
            'userHour' => ['type' => 'integer', 'min' => 0, 'max' => 24, 'required' => TRUE],
        ];
    }
    
    private function getDataFromOut($attrName, $setvalue)
    {
        if (isset($this->attributes()[$attrName]) && isset($this->attributes()[$attrName]['type'])) {
            return filter_var($setvalue, 
                     (isset($this->sanitaze()[$this->attributes()[$attrName]['type']]) ? $this->sanitaze()[$this->attributes()[$attrName]['type']] : FILTER_SANITIZE_STRING));
        }
        return filter_var($setvalue, FILTER_SANITIZE_STRING);
    }   
    
    private function addError($attrName, $errorMessage)
    {
        $this->errors[$attrName] = $errorMessage;
    }
            
    private function clearErrors()
    {
        $this->errors = [];
    }
    
    private function sanitaze() {
        return [
            'integer' => FILTER_VALIDATE_INT,
            'number' => FILTER_VALIDATE_FLOAT,
            'string' => FILTER_SANITIZE_STRING
        ];
    }
}

