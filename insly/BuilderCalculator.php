<?php
namespace insly;

use insly\Calculator;

class BuilderCalculator
{
    public $estimate;
    public $tax;
    public $commition;
    public $instalments;
    public $basePrice;
    public $userDay;
    public $userHour;
    public $extendPrice = null;
    public $extendPriceHourFrom = null;
    public $extendPriceHourTo = null;
    public $extendPriceDay = null;
    
    public function setEstimate($estimate)
    {
        $this->estimate = (int)$estimate;
        return $this;
    }
    
    public function setTax($tax)
    {
        $this->tax = (int)$tax;
        return $this;
    }
    
    public function setCommition($commition)
    {
        $this->commition = (int)$commition;
        return $this;
    }
    public function setInstalments($instalments)
    {
        $this->instalments = (int)$instalments;
        return $this;
    }
    
    public function setBasePrice($basePrice)
    {
        $this->basePrice = (int)$basePrice;
        return $this;
    }
    
    public function setUserDay($userDay)
    {
        $this->userDay = (int)$userDay;
        return $this;
    }

    public function setUserHour($userHour)
    {
        $this->userHour = (int)$userHour;
        return $this;
    }
    
    public function setExtendPrice($extendPrice)
    {
        $this->extendPrice = (int)$extendPrice;
        return $this;
    }
    
    public function setExtendPriceHourFrom($extendPriceHourFrom)
    {
        $this->extendPriceHourFrom = (int)$extendPriceHourFrom;
        return $this;
    }
    
    public function setExtendPriceHourTo($extendPriceHourTo)
    {
        $this->extendPriceHourTo = (int)$extendPriceHourTo;
        return $this;
    }
    
    public function setExtendPriceDay($extendPriceDay)
    {
        $this->extendPriceDay = (int)$extendPriceDay;
        return $this;
    }

    public function getCalculator()
    {
        return new Calculator($this);
    }

}
