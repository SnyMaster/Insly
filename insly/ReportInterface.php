<?php

namespace insly;

interface ReportInterface
{
    public function renderReport(array $data);
}
