<?php
namespace insly;

use insly\ReportInterface;

class ReportCalculator implements ReportInterface
{
 
    public function renderReport(array $data) {
        $report = '<table class="report-calculator"><tr><th>&nbsp;</th><th>Policy</th>';
        for ($i = 1; $i <= count($data['price']); $i++) {
            $report .= "<th> $i Instalment </th>";
        }
        $report .= '</th></tr><tr><td>Value</td><td  class="sum">' . number_format($data['estimate'], 2) . '</td>';
        for ($i = 1; $i <= count($data['price']); $i++) {
            $report .= "<td>&nbsp;</td>";
        }
        $report .= '</tr><tr><td>Base premium (' . $data['pricePercent'] . '%)</td><td class="sum">' . number_format(array_sum($data['price']), 2) . '</td>';
        foreach ($data['price'] as $val) {
            $report .= '<td class="sum">' . number_format($val, 2) . '</td>';
        }
        $report .= '</tr><tr><td>Comission (' . $data['commitionPercent'] . '%)</td><td class="sum">' . number_format(array_sum($data['commition']), 2) . '</td>';
        foreach ($data['commition'] as $val) {
            $report .= '<td class="sum">' . number_format($val, 2) . '</td>';
        }
        $report .= '</tr><tr><td>Tax (' . $data['taxPercent'] . '%)</td><td class="sum">' . number_format(array_sum($data['tax']), 2) . '</td>';
        foreach ($data['tax'] as $val) {
            $report .= '<td class="sum">' . number_format($val, 2) . '</td>';
        }
        $report .= '</tr><tr><td><strong>Total cost</strong></td><td class="sum"><strong>' . 
                number_format(($data['estimate'] + array_sum($data['price']) + array_sum($data['commition']) + array_sum($data['tax'])), 2) . 
                '</strong></td>';
        for ($i = 0; $i < count($data['price']); $i++) {
            $total = $data['price'][$i] + $data['commition'][$i] + $data['tax'][$i];
            $report .= '<td class="sum">' .number_format($total, 2) . '</td>';
        }
        $report .= '</tr></table>';
        return  $report;
    }
}

