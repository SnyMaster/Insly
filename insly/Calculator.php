<?php
namespace insly;
use insly\BuilderCalculator;

class Calculator 
{
    private $estimate;
    private $tax;
    private $commition;
    private $instalments;
    private $basePrice;
    private $userDay;
    private $userHour;
    private $extendPrice;
    private $extendPriceHourFrom;
    private $extendPriceHourTo;
    private $extendPriceDay;
    
    public function __construct(BuilderCalculator $builder)
    {    
        $this->estimate = $builder->estimate;
        $this->tax = $builder->tax;
        $this->commition = $builder->commition;
        $this->instalments = $builder->instalments;
        $this->basePrice = $builder->basePrice;
        $this->userDay = $builder->userDay;
        $this->userHour = $builder->userHour;  
        $this->extendPrice = $builder->extendPrice;
        $this->extendPriceDay = $builder->extendPriceDay;
        $this->extendPriceHourFrom = $builder->extendPriceHourFrom;
        $this->extendPriceHourTo = $builder->extendPriceHourTo;
    }
    
    public function calculate(){
        $calcResult['estimate'] = $this->estimate;
        $calcResult['price'] = $this->calculatePrice();
        $calcResult['commition'] = $this->calculateCommition(array_sum($calcResult['price']));
        $calcResult['tax'] = $this->calculateTax(array_sum($calcResult['price']));
        $calcResult['pricePercent'] = $this->getCalcPrice();
        $calcResult['commitionPercent'] = $this->commition;
        $calcResult['taxPercent'] = $this->tax;
        
        return $calcResult;
    }
    
    public function calculatePrice()
    {
        $calcPrice = $this->getCalcPrice();
        $price = $this->estimate * ($calcPrice / 100);
        return $this->splitSum($price);
    }
    
    public function calculateCommition($price)
    {
        $comm = round($price * ($this->commition / 100), 2);
        return $this->splitSum($comm);
    }
    
    public function calculateTax($price)
    {
        $tax = round($price * ($this->tax/ 100));
        return $this->splitSum($tax);
    }
    
    public function getCalcPrice()
    {
        return ((int)$this->userDay === $this->extendPriceDay && 
                (int)$this->userHour >= (int)$this->extendPriceHourFrom && 
                (int)$this->userHour <= (int)$this->extendPriceHourTo) ?
                $this->extendPrice : $this->basePrice;
    }
    
    private function splitSum($sum)
    {
        $sumPart = $sum / $this->instalments;
        if ($sum === round($sumPart, 2) * $this->instalments) {
            return array_fill(0, $this->instalments, round($sumPart, 2));
        }
        $countDown = intval(explode('.', number_format(round($sumPart, 2) * $this->instalments - $sum, 2))[1]);
        $countUp = $this->instalments - $countDown;
        $arrUpDown = round($sumPart, 2) * $this->instalments - $sum < 0 ? array_fill($countUp, $countDown, ceil($sumPart * 100) / 100) : array_fill($countUp, $countDown, floor($sumPart * 100) / 100);
        return array_fill(0, $countUp, round($sumPart, 2)) + $arrUpDown;
    }
    
}
