<?php

use insly\Form;
use insly\BuilderCalculator;
use insly\ReportCalculator;

spl_autoload_extensions(".php");
spl_autoload_register();

if (!empty($_POST)) {
    $formData = new Form();
    
    if ($formData->load($_POST)) {
        $calc = (new BuilderCalculator())->setEstimate($formData->getEstimate())
                ->setTax($formData->getTax())
                ->setInstalments($formData->getInstalments())
                ->setBasePrice(11)
                ->setUserDay($formData->getUserDay())
                ->setUserHour($formData->getUserHour())
                ->setCommition(17)
                ->setExtendPrice(20)
                ->setExtendPriceDay(5)
                ->setExtendPriceHourFrom(15)
                ->setExtendPriceHourTo(20)
                ->getCalculator();
        echo (new ReportCalculator())->renderReport($calc->calculate());
    } else {
        echo '<p class="error-message">' . implode('<br/>', $formData->getErrors()) . '</p>';
    }
}